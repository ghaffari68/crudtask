﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLayer.Contracts;
using DataLayer.Models;
using DataLayer.Repositories;

namespace DataLayer.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private IOwnerRepository _ownerRepository;
        private IEstateRepository _estateRepository;
        public EstateContext _Context { get; }


        public UnitOfWork(EstateContext context)
        {
            _Context = context;
        }

        public IEstateRepository EstateRepository
        {
            get
            {
                if (_estateRepository == null)
                    _estateRepository = new EstateRepository(_Context);

                return _estateRepository;
            }
        }

        public IOwnerRepository OwnerRepository
        {
            get
            {
                if (_ownerRepository == null)
                    _ownerRepository = new OwnerRepository(_Context);

                return _ownerRepository;
            }
        }

        public async Task Commit()
        {
            await _Context.SaveChangesAsync();
        }
    }
}
