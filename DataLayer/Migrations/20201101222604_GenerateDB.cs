﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class GenerateDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Owner",
                columns: table => new
                {
                    OwnerId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Family = table.Column<string>(maxLength: 50, nullable: true),
                    PhoneNumber = table.Column<string>(unicode: false, maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Owner", x => x.OwnerId);
                });

            migrationBuilder.CreateTable(
                name: "Estate",
                columns: table => new
                {
                    EstateId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OwnerId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Area = table.Column<double>(nullable: false),
                    Address = table.Column<string>(nullable: false),
                    Position = table.Column<string>(maxLength: 10, nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    ManipulatorUserId = table.Column<int>(nullable: false),
                    CreationTime = table.Column<string>(nullable: true),
                    UpdateTime = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Estate", x => x.EstateId);
                    table.ForeignKey(
                        name: "FK_Estate_Owner_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "Owner",
                        principalColumn: "OwnerId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Owner",
                columns: new[] { "OwnerId", "Family", "Name", "PhoneNumber" },
                values: new object[] { 1, "محمدی", "علی", "0912222333" });

            migrationBuilder.InsertData(
                table: "Owner",
                columns: new[] { "OwnerId", "Family", "Name", "PhoneNumber" },
                values: new object[] { 2, "احمدی", "رضا", "0926542586" });

            migrationBuilder.InsertData(
                table: "Owner",
                columns: new[] { "OwnerId", "Family", "Name", "PhoneNumber" },
                values: new object[] { 3, "اترج", "محسن", "0913652333" });

            migrationBuilder.CreateIndex(
                name: "IX_Estate_OwnerId",
                table: "Estate",
                column: "OwnerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Estate");

            migrationBuilder.DropTable(
                name: "Owner");
        }
    }
}
