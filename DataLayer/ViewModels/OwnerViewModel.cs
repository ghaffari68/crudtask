﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.ViewModels
{
    public class OwnerViewModel
    {
        public int OwnerId { get; set; }
        public string FullName { get; set; }
    }
}
