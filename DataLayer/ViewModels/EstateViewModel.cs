﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DataLayer.ViewModels
{
    public class EstateViewModel
    {
        public int EstateId { get; set; }
        [Required(ErrorMessage = "لطفا {0} را انتخاب کنید")]
        [Display(Name = "نام مالک")]
        public int OwnerId { get; set; }
        
        [Display(Name = "نام ملک")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        public string Name { get; set; }
        [Display(Name = "نام مالک")]
        public string OwnerName { get; set; }
        [Display(Name = "شماره تماس مالک")]
        public string PhoneNumber { get; set; }
        [Display(Name = " متراژ (متر)")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        public string Area { get; set; }
        [Display(Name = "آدرس")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        public string Address { get; set; }
        [Display(Name = "موقعیت ")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        public string Position { get; set; }
        public string CreationTime { get; set; }
    }
}
