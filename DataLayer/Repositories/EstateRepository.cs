﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Contracts;
using DataLayer.Models;
using DataLayer.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace DataLayer.Repositories
{
    public class EstateRepository : IEstateRepository
    {
        private readonly EstateContext _estateContext;

        public EstateRepository(EstateContext estateContext)
        {
            this._estateContext = estateContext;
        }
        public async Task<List<EstateViewModel>> GetAllEstates()
        {
            var estates = _estateContext.Estate.Include(o => o.Owner).Where(e => !e.IsDeleted).Select(e => new EstateViewModel()
            {
                EstateId = e.EstateId,
                OwnerId = e.OwnerId,
                Name = e.Name,
                OwnerName = string.Format("{0}  {1}", e.Owner.Name, e.Owner.Family),
                PhoneNumber = e.Owner.PhoneNumber,
                Area = e.Area.ToString(),
                Address = e.Address,
                Position = e.Position
            });
            return await estates.ToListAsync();
        }

        public async Task<Estate> GetEstateById(int estateId)
        {
            return await _estateContext.Estate.Include(o=> o.Owner).FirstOrDefaultAsync(e => e.EstateId == estateId);
        }

        public async Task<bool> InsertEstate(Estate estate)
        {
            try
            {
                await _estateContext.Estate.AddAsync(estate);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool UpdateEstate(Estate estate)
        {
            try
            {
                _estateContext.Update(estate);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool DeleteEstate(Estate estate)
        {
            try
            {
                estate.IsDeleted = true;
                _estateContext.Update(estate);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public async Task<bool> DeleteEstate(int estateId)
        {
            try
            {
                var estate = await GetEstateById(estateId);
                DeleteEstate(estate);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool IsExist(int estateId)
        {
           return _estateContext.Estate.Any(e => e.EstateId == estateId);
        }


        public void Dispose()
        {
          _estateContext.Dispose();
        }
    }
}
