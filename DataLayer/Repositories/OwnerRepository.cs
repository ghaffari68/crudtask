﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLayer.Contracts;
using DataLayer.Models;
using DataLayer.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace DataLayer.Repositories
{
    public class OwnerRepository:IOwnerRepository
    {
        private readonly EstateContext _estateContext;

        public OwnerRepository(EstateContext estateContext)
        {
            this._estateContext = estateContext;
        }

        public IEnumerable<OwnerViewModel> GetAllOwners()
        {
            return _estateContext.Owner.Select(o => new OwnerViewModel()
                {
                    OwnerId = o.OwnerId,
                    FullName = string.Format("{0}  {1}", o.Name, o.Family)
                }).ToList();
        }

        public void Dispose()
        {
            _estateContext.Dispose();
        }
    }
}
