﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;
using DataLayer.ViewModels;

namespace DataLayer.Contracts
{
    public interface IOwnerRepository:IDisposable
    {
        IEnumerable<OwnerViewModel> GetAllOwners();
    }
}
