﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;
using DataLayer.ViewModels;

namespace DataLayer.Contracts
{
    public interface IEstateRepository:IDisposable
    {
        Task<List<EstateViewModel>> GetAllEstates();
        Task<Estate> GetEstateById(int estateId);
        Task<bool> InsertEstate(Estate estate);
        bool UpdateEstate(Estate estate);
        bool DeleteEstate(Estate estate);
        Task<bool> DeleteEstate(int estateId);
        bool IsExist(int estateId);
    }
}
