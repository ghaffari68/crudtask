﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLayer;
using DataLayer.Models;

namespace DataLayer.Contracts
{
    public interface IUnitOfWork
    {
        IEstateRepository EstateRepository { get; }
        IOwnerRepository OwnerRepository { get; }
        EstateContext _Context { get; }
        Task Commit();
    }
}
