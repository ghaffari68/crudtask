﻿using System;
using System.Collections.Generic;
using System.Text;
using DataLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.Mapping
{
    public class OwnerMap : IEntityTypeConfiguration<Owner>
    {
        public void Configure(EntityTypeBuilder<Owner> entity)
        {
            entity.HasKey(e => e.OwnerId);
            entity.Property(e => e.Family).HasMaxLength(50);

            entity.Property(e => e.Name).HasMaxLength(50);

            entity.Property(e => e.PhoneNumber)
                .HasMaxLength(20)
                .IsUnicode(false);

            entity.HasData(
                new Owner() {OwnerId = 1, Name = "علی", Family = "محمدی", PhoneNumber = "09124225638"},
                new Owner() {OwnerId = 2, Name = "رضا", Family = "احمدی", PhoneNumber = "09903651214"},
                new Owner() {OwnerId = 3, Name = "محسن", Family = "اترج", PhoneNumber = "09132876154"});
        }
    }
}