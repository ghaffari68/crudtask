﻿using System;
using System.Collections.Generic;
using System.Text;
using DataLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.Mapping
{
    public class EstateMap : IEntityTypeConfiguration<Estate>
    {
        public void Configure(EntityTypeBuilder<Estate> entity)
        {
            entity.HasKey(e => e.EstateId);
            entity.Property(e => e.EstateId).ValueGeneratedOnAdd();
            entity.Property(e => e.Address).IsRequired();

            entity.Property(e => e.IsDeleted).HasDefaultValue(false);

            entity.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(50);

            entity.Property(e => e.OwnerId).HasColumnName("OwnerId").IsRequired();

            entity.Property(e => e.Position)
                .IsRequired()
                .HasMaxLength(10);

            entity.HasOne(d => d.Owner)
                .WithMany(p => p.Estate)
                .HasForeignKey(d => d.OwnerId);
        }
    }
}