﻿using System;
using System.Collections.Generic;

namespace DataLayer.Models
{
    public class Estate
    {
        public int EstateId { get; set; }
        public int OwnerId { get; set; }
        public string Name { get; set; }
        public double Area { get; set; }
        public string Address { get; set; }
        public string Position { get; set; }
        public bool IsDeleted { get; set; }
        public int ManipulatorUserId { get; set; }
        public string CreationTime { get; set; }
        public string UpdateTime { get; set; }

        public virtual Owner Owner { get; set; }
    }
}
