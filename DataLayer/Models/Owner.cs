﻿using System;
using System.Collections.Generic;

namespace DataLayer.Models
{
    public class Owner
    {
        public Owner()
        {
            Estate = new HashSet<Estate>();
        }

        public int OwnerId { get; set; }
        public string Name { get; set; }
        public string Family { get; set; }
        public string PhoneNumber { get; set; }

        public virtual ICollection<Estate> Estate { get; set; }
    }
}
