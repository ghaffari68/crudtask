﻿using System;
using DataLayer.Mapping;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DataLayer.Models
{
    public class EstateContext : DbContext
    {
        public EstateContext(DbContextOptions<EstateContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Estate> Estate { get; set; }
        public virtual DbSet<Owner> Owner { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new OwnerMap());
            modelBuilder.ApplyConfiguration(new EstateMap());
        }
    }
}