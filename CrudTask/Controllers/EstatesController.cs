﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLayer;
using DataLayer.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DataLayer.Models;
using DataLayer.UnitOfWork;
using DataLayer.ViewModels;

namespace CrudTask.Controllers
{
    public class EstatesController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public EstatesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: Estates
        public async Task<IActionResult> Index()
        {
            var estates = await _unitOfWork.EstateRepository.GetAllEstates();
            return View(estates);
        }


        // GET: Estates/Create
        public IActionResult Create()
        {
            var owners = _unitOfWork.OwnerRepository.GetAllOwners();
            var positions = new List<string> {"شمالی", "جنوبی"};
            ViewBag.OwnerId = new SelectList(owners, "OwnerId", "FullName");
            ViewBag.Position = new SelectList(positions);
            return View();
        }

        // POST: Estates/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("OwnerId,Name,Area,Address,Position")]
            EstateViewModel estateViewModel)
        {
            if (ModelState.IsValid)
            {
                var estate = new Estate()
                {
                    OwnerId = estateViewModel.OwnerId,
                    Name = estateViewModel.Name,
                    Area = Convert.ToDouble(estateViewModel.Area),
                    Position = estateViewModel.Position,
                    Address = estateViewModel.Address,
                    //ToDo: Get Current User Id from UserManager Repository. At now we assume that is 2
                    ManipulatorUserId = 2,
                    CreationTime = DateTime.Now.ToString("MM/dd/yyyy HH:mm")
                };
                await _unitOfWork.EstateRepository.InsertEstate(estate);
                await _unitOfWork.Commit();
                return RedirectToAction(nameof(Index));
            }

            var owners = _unitOfWork.OwnerRepository.GetAllOwners();
            var positions = new List<string> {"شمالی", "جنوبی"};
            ViewBag.OwnerId = new SelectList(owners, "OwnerId", "FullName");
            ViewBag.Position = new SelectList(positions);

            return View(estateViewModel);
        }

        // GET: Estates/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            var estate = await _unitOfWork.EstateRepository.GetEstateById(id);
            if (estate == null)
            {
                return NotFound();
            }

            var estateViewModel = new EstateViewModel()
            {
                EstateId = estate.EstateId,
                OwnerId = estate.OwnerId,
                Name = estate.Name,
                Area = estate.Area.ToString(),
                Position = estate.Position,
                Address = estate.Address,
                CreationTime = estate.CreationTime
            };
            var owners = _unitOfWork.OwnerRepository.GetAllOwners();
            var positions = new List<string> {"شمالی", "جنوبی"};
            ViewBag.OwnerId = new SelectList(owners, "OwnerId", "FullName", estate.OwnerId);
            ViewBag.Position = new SelectList(positions, estate.Position);

            return View(estateViewModel);
        }

        // POST: Estates/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,
            [Bind("EstateId,OwnerId,Name,Area,Address,Position,CreationTime")]
            EstateViewModel estateViewModel)
        {
            if (id != estateViewModel.EstateId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var estate = new Estate()
                    {
                        EstateId = estateViewModel.EstateId,
                        OwnerId = estateViewModel.OwnerId,
                        Name = estateViewModel.Name,
                        Area = Convert.ToDouble(estateViewModel.Area),
                        Position = estateViewModel.Position,
                        Address = estateViewModel.Address,
                        //ToDo: Get Current User Id from UserManager Repository. At now we assume that is 2
                        ManipulatorUserId = 2,
                        CreationTime = estateViewModel.CreationTime,
                        UpdateTime = DateTime.Now.ToString("MM/dd/yyyy HH:mm")
                    };
                    _unitOfWork.EstateRepository.UpdateEstate(estate);
                    await _unitOfWork.Commit();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EstateExists(estateViewModel.EstateId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToAction(nameof(Index));
            }

            var owners = _unitOfWork.OwnerRepository.GetAllOwners();
            var positions = new List<string> {"شمالی", "جنوبی"};
            ViewBag.OwnerId = new SelectList(owners, "OwnerId", "FullName", estateViewModel.OwnerId);
            ViewBag.Position = new SelectList(positions, estateViewModel.Position);
            return View(estateViewModel);
        }

        // GET: Estates/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            var estate = await _unitOfWork.EstateRepository.GetEstateById(id);
            if (estate == null)
            {
                return NotFound();
            }

            var estateViewModel = new EstateViewModel()
            {
                EstateId = estate.EstateId,
                OwnerId = estate.OwnerId,
                Name = estate.Name,
                OwnerName = string.Format("{0}  {1}", estate.Owner.Name, estate.Owner.Family),
                PhoneNumber = estate.Owner.PhoneNumber,
                Area = estate.Area.ToString(),
                Address = estate.Address,
                Position = estate.Position
            };

            return View(estateViewModel);
        }

        // POST: Estates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _unitOfWork.EstateRepository.DeleteEstate(id);
            await _unitOfWork.Commit();
            return RedirectToAction(nameof(Index));
        }

        // GET: Estates/Details/5
        public async Task<IActionResult> Details(int id)
        {
            var estate = await _unitOfWork.EstateRepository.GetEstateById(id);
            if (estate == null)
            {
                return NotFound();
            }

            var estateViewModel = new EstateViewModel()
            {
                EstateId = estate.EstateId,
                OwnerId = estate.OwnerId,
                Name = estate.Name,
                OwnerName = string.Format("{0}  {1}", estate.Owner.Name, estate.Owner.Family),
                PhoneNumber = estate.Owner.PhoneNumber,
                Area = estate.Area.ToString(),
                Address = estate.Address,
                Position = estate.Position
            };

            return View(estateViewModel);
        }

        private bool EstateExists(int id)
        {
            return _unitOfWork.EstateRepository.IsExist(id);
        }
    }
}