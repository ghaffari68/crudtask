# CrudTask


- This is an interview task based on **repository pattern**.

- Since I'm a backend developer, I used **default layout** and my focus was on **backend**.

- There is no login page in this task and for recording user log in Estate Table I assumed userId is 2 as hard code.
- Based on defining of task, Owner table already initialized with some records and CRUD just do on Estate table.

## Tables
Estate, Owner

## Structres

1. **CrudTask**: This is an Asp.Net Core MVC project based on latest version of .net core (3.1)</br>
2. **DataLayer**: This is a class library including:</br>

- _Models_: EstateDBContext and Database entities</br>

- _Mappings_: Model mapping for code first Entity Framework Core</br>

- _Contracts_: Repositories interfaces</br>

- _Repositories_: OwnerRepository and EstateRepository (There is no need to using Generic Repository since we had just one entity to CRUD)</br>

- _UnitOfWork_: To manage Repositories I used UnitOfWork pattern</br>
- _ViewModels_: To show data and deal with forms in views </br>
- _Migrations_: To create and update database based on Package Manager Console



